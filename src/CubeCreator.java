import java.util.ArrayList;
import javax.media.j3d.Appearance;
import javax.media.j3d.GeometryArray;
import javax.media.j3d.ImageComponent2D;
import javax.media.j3d.LineStripArray;
import javax.media.j3d.Shape3D;
import javax.media.j3d.Texture;
import javax.media.j3d.Texture2D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import com.sun.j3d.utils.image.TextureLoader;
import javax.media.j3d.Transform3D;

/**
 *
 * @author Małgorzata
 * 
 * Klasa odpowiadająca za tworzenie kostki Rubika.
 * 
 */

public class CubeCreator {
    
    public int nr;
    public int x;
    public int y;
    public int z;
    
       
    CubeCreator(ArrayList<TransformGroup> kosteczki, int[][][] kostka){
        
        nr = 0;
        x = 0;
        y = 0;
        z = 0;
        
        // Tworzenie kostki Rubika
        for (double i = -0.101; i < 0.202; i+=0.101) {
            for (double j = -0.101; j < 0.202; j+=0.101) {  
                for (double k = -0.101; k < 0.202; k+=0.101) {
                    TransformGroup bigCube = new TransformGroup();
                    bigCube.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
                    bigCube.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
                    Transform3D smallCube = new Transform3D();
                    smallCube.setTranslation(new Vector3d(i,j,k));
                    bigCube.setTransform(smallCube);
                    
                    // Tworzenie kosteczek, z których składa się kostka i nakładanie na nie tekstur
                    MyCube newCube = new MyCube(0.051f, nr, MyCube.GENERATE_NORMALS | MyCube.GENERATE_TEXTURE_COORDS, null);
                    newCube.setAppearance(MyCube.TOP, CubeWalls(new TextureLoader("Textures/purple.png",null)));
                    newCube.setAppearance(MyCube.BOTTOM, CubeWalls(new TextureLoader("Textures/darkyellow.png", null)));
                    newCube.setAppearance(MyCube.RIGHT, CubeWalls(new TextureLoader("Textures/red.png", null)));
                    newCube.setAppearance(MyCube.LEFT, CubeWalls(new TextureLoader("Textures/orange.png", null)));
                    newCube.setAppearance(MyCube.FRONT, CubeWalls(new TextureLoader("Textures/green.png", null)));
                    newCube.setAppearance(MyCube.BACK, CubeWalls(new TextureLoader("Textures/blue.png", null)));
                    
                    // Dodawanie kosteczek do kostki Rubika
                    bigCube.addChild(newCube);
                    
                    // Zapisywanie kostek do listy TranformGroup'ów,
                    // na których wykonywane będzie obracanie kostek
                    TransformGroup bigCube2 = new TransformGroup();
                    bigCube2.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
                    bigCube2.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
                    bigCube2.addChild(bigCube);
                    kosteczki.add(bigCube2);

                    // Wpisanie elementów kostki do tablicy
                    // oraz przypisanie im charakterystycznych numerów
                    kostka[x][y][z] = nr;

                    nr++;
                    z++;

                    // Rysowanie osi współrzędnych
                    if(nr == 20) {
                        Axis(0.5d, 0.0d, 0.0d, bigCube);
                        Axis(0.0d, 0.5d, 0.0d, bigCube);
                        Axis(0.0d, 0.0d, 0.5d, bigCube);
                    }
                }
                z = 0;
                y++;
            }
            y = 0;
            x++;
        } 
    }
    
    // Metoda odpowiadająca za wczytywanie i ustawianie tekstur
    public static Appearance CubeWalls(TextureLoader loader){
        Appearance appearance = new Appearance();
        ImageComponent2D appImage = loader.getImage();
        Texture2D appTex = new Texture2D(Texture.BASE_LEVEL, Texture.RGBA, 
                            appImage.getWidth(), appImage.getHeight());
        appTex.setImage(0, appImage);
        appTex.setBoundaryModeS(Texture.WRAP);
        appTex.setBoundaryModeT(Texture.WRAP);
        appearance.setTexture(appTex);
        
        return appearance;
    }

    // Metoda odpowiadająca za rysowanie pomocniczych osi współrzędnych
    private static void Axis(double d1, double d2, double d3, TransformGroup G_kostka) {
        Point3d coords[] = new Point3d[2];
        Appearance app=new Appearance();
        int vertexCounts[] = {2};
        coords[0] = new Point3d(0.0d, 0.0d, 0.0d);
        coords[1] = new Point3d(d1, d2, d3);

        LineStripArray lines = new LineStripArray(2, GeometryArray.COORDINATES, vertexCounts);
        lines.setCoordinates(0, coords);
        Shape3D shape = new Shape3D(lines , app);
        G_kostka.addChild(shape);
    }
}
