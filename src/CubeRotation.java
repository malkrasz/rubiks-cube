import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.AxisAngle4d;

/**
 *
 * @author Kamil
 * 
 * Klasa odpowiadająca za obracanie elementów kostki.
 * 
 */
public class CubeRotation {
    
    private int[][][] kostka;
    private ArrayList<TransformGroup> kosteczki;
    public ArrayList<RotationData> daneObrotow = new ArrayList<RotationData>();
    MyCube tempC;
    
    CubeRotation(int[][][] kostka,ArrayList<TransformGroup> kosteczki ){
        this.kostka = kostka;
        this.kosteczki = kosteczki;
        
        // Inicjalizacja danych niezbędnych do procesu
        // obracania warstw kostki
        daneObrotow.add(new RotationData("R", 'x', 2, -1));
        daneObrotow.add(new RotationData("B", 'y', 2, -1));
        daneObrotow.add(new RotationData("U", 'z', 2, -1));
        daneObrotow.add(new RotationData("L", 'x', 0, 1));
        daneObrotow.add(new RotationData("F", 'y', 0, 1));
        daneObrotow.add(new RotationData("D", 'z', 0, 1));
    }
    
    
    // Metoda zawierająca implementację procesu obrotu
    public void obrocWarstwe(String akcja, double kat, boolean zapiszObrot){
        
        TransformGroup temp;
        TransformGroup tempCube;
        
        // Obrót prawej warstwy kostki
        if("R".equals(akcja) | "R'".equals(akcja)){
            if("R'".equals(akcja)) kat = -kat;
            
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    
                    // Wczytanie elementów kostki, z których składa się obracana warstwa
                    temp = kosteczki.get(kostka[2][i][j]);
                    tempCube = (TransformGroup)(temp.getChild(0));
                    tempC = (MyCube)tempCube.getChild(0);

                    // Zapisywanie danych wykonywanego obrotu
                    // oraz obrót kosteczek
                    if(zapiszObrot){
                        tempC.katObrotu.add(-kat);
                        tempC.ktoraOs.add('x');
                        tempC.obrot.add('R');
                        rotateCube(tempC, temp);
                    }
                    
                    // Warunek odpowiadający za obroty o mały kąt wynikające
                    // z animacji obrotu warstw przy pomocy myszki
                    else{
                        Transform3D transform = new Transform3D();
                        transform.setRotation(new AxisAngle4d(1, 0 ,0, kat));
                        rotateCube(tempC, temp, transform);
                    }
                }
            }
        }
        
        // Obrót dolnej warstwy kostki
        else if("U".equals(akcja) | "U'".equals(akcja)){
            if("U'".equals(akcja)) kat = -kat;
            
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    temp = kosteczki.get(kostka[i][j][2]);
                    tempCube = (TransformGroup)(temp.getChild(0));
                    tempC = (MyCube)tempCube.getChild(0);

                    if(zapiszObrot){
                        tempC.katObrotu.add(-kat);
                        tempC.ktoraOs.add('z');
                        tempC.obrot.add('U');
                        rotateCube(tempC, temp);
                    }
                    else{
                        Transform3D transform = new Transform3D();
                        transform.setRotation(new AxisAngle4d(0, 0, 1, kat));
                        rotateCube(tempC, temp, transform);
                    }
                }
            }
        }
        
        // Obrót dolnej warstwy kostki
        else if("D".equals(akcja) | "D'".equals(akcja)){
            if("D'".equals(akcja)) kat = -kat;
            
            for (int i = 0; i < 3; i++) {
                 for (int j = 0; j < 3; j++) {
                    temp = kosteczki.get(kostka[i][j][0]);
                    tempCube = (TransformGroup)(temp.getChild(0));
                    tempC = (MyCube)tempCube.getChild(0);
                    
                    if(zapiszObrot){
                        tempC.katObrotu.add(kat);
                        tempC.ktoraOs.add('z');
                        tempC.obrot.add('D');
                        rotateCube(tempC, temp);
                    }
                    else{
                        Transform3D transform = new Transform3D();
                        transform.setRotation(new AxisAngle4d(0, 0, 1, kat));
                        rotateCube(tempC, temp, transform);
                    }
               }
           }
        }
        
        // Obrót lewej warstwy kostki
        else if("L".equals(akcja) | "L'".equals(akcja)){
            if("L'".equals(akcja)) kat = -kat;
            
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    temp = kosteczki.get(kostka[0][i][j]);
                    tempCube = (TransformGroup)(temp.getChild(0));
                    tempC = (MyCube)tempCube.getChild(0);

                    if(zapiszObrot){
                        tempC.katObrotu.add(kat);
                        tempC.ktoraOs.add('x');
                        tempC.obrot.add('L');
                        rotateCube(tempC, temp);
                    }
                    else{
                        Transform3D transform = new Transform3D();
                        transform.setRotation(new AxisAngle4d(1, 0, 0, kat));
                        rotateCube(tempC, temp, transform);
                    }
                }
            }
        }
        
        // Obrót przedniej warstwy kostki
        else if("F".equals(akcja) | "F'".equals(akcja)){
            if("F'".equals(akcja)) kat = -kat;
            
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    temp = kosteczki.get(kostka[i][0][j]);
                    tempCube = (TransformGroup)(temp.getChild(0));
                    tempC = (MyCube)tempCube.getChild(0);

                    if(zapiszObrot){
                        tempC.katObrotu.add(kat);
                        tempC.ktoraOs.add('y');
                        tempC.obrot.add('F');
                        rotateCube(tempC, temp);
                    }
                    else{
                        Transform3D transform = new Transform3D();
                        transform.setRotation(new AxisAngle4d(0, 1, 0, kat));
                        rotateCube(tempC, temp, transform);
                    }
                }
            }
        }
        
        // Obrót tylnej warstwy kostki
        else if("B".equals(akcja) | "B'".equals(akcja)){
            if("B'".equals(akcja)) kat = -kat;
            
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    temp = kosteczki.get(kostka[i][2][j]);
                    tempCube = (TransformGroup)(temp.getChild(0));
                    tempC = (MyCube)tempCube.getChild(0);

                    if(zapiszObrot){
                        tempC.katObrotu.add(-kat);
                        tempC.ktoraOs.add('y');
                        tempC.obrot.add('B');
                        rotateCube(tempC, temp);
                    }
                    else{
                        Transform3D transform = new Transform3D();
                        transform.setRotation(new AxisAngle4d(0, 1, 0, kat));
                        rotateCube(tempC, temp, transform);
                    }
                }
            }
        }
    }
    
    public void previousMove(AbstractCube abstractCube){
        char akcja = tempC.obrot.lastElement();
        if(Character.toString(akcja) == "L"){
            obrocWarstwe("L'", Math.PI/2, false);
            abstractCube.Rotation("L'");
        }
        else if(Character.toString(akcja) == "U"){
            obrocWarstwe("U'", Math.PI/2, false);
            abstractCube.Rotation("U'");
        }
         else if(Character.toString(akcja) == "B"){
            obrocWarstwe("B'", Math.PI/2, false);
            abstractCube.Rotation("B'");
         }
         else if(Character.toString(akcja) == "D"){
            obrocWarstwe("D'", Math.PI/2, false);
            abstractCube.Rotation("D'");
         }
         else if(Character.toString(akcja) == "F"){
            obrocWarstwe("F'", Math.PI/2, false);
            abstractCube.Rotation("F'");
         }
         else if(Character.toString(akcja) == "R"){
            obrocWarstwe("R'", Math.PI/2, false);
            abstractCube.Rotation("R'");
         }
        //obrocWarstwe(akcja + "'", Math.PI/2, false);
        //tempC.katObrotu.removeElementAt(tempC.katObrotu.size()-1);
        //tempC.ktoraOs.removeElementAt(tempC.ktoraOs.size()-1);
        //tempC.obrot.removeElementAt(tempC.obrot.size()-1);
        
    }
    
    
    // Metoda odpowiadająca za właściwy obrót kosteczek
    public void rotateCubeCore(MyCube cube, TransformGroup rotation, Transform3D obrCal){
        Transform3D transformCube = new Transform3D();

        // Możenie macierzy w kolejności odwrotnej, aniżeli kolejność,
        // która wynika z prawa składania obrotów
        for(int n = cube.katObrotu.size()-1; n >= 0; n--){

            // Obrót kosteczki wokół osi x
            if(cube.ktoraOs.get(n) == 'x')
                transformCube.setRotation(new AxisAngle4d(1,0,0,cube.katObrotu.get(n)));

            // Obrót kosteczki wokół osi y
            else if(cube.ktoraOs.get(n) == 'y')
                transformCube.setRotation(new AxisAngle4d(0,1,0,cube.katObrotu.get(n)));

            // Obrót kosteczki wokół osi z
            else if(cube.ktoraOs.get(n) == 'z')
                 transformCube.setRotation(new AxisAngle4d(0,0,1,cube.katObrotu.get(n)));

            obrCal.mul(transformCube);
        }  
        
        rotation.setTransform(obrCal);
    }
    
    
    public void rotateCube(MyCube cube, TransformGroup rotation){
        Transform3D obrCal = new Transform3D();
        rotateCubeCore(cube, rotation, obrCal);
    }
    
    
    public void rotateCube(MyCube cube, TransformGroup rotation,Transform3D obrCal){
        rotateCubeCore(cube, rotation, obrCal);
    }
    
    
    // Metoda odpowiada za "tasowanie" kostki Rubika
    public void Shuffle(AbstractCube abstractCube){
        String akcja = null;
        Random randomRot = new Random();
        Vector<Integer> rot = new Vector<Integer>();
        
        for(int i = 0; i < 100; i++){
            int rotation = randomRot.nextInt(6);
            rot.add(rotation);
        }
        
        for(int i = rot.size()-1; i >= 0; i--) {
            if(rot.get(i) == 0) akcja = "R";
            else if(rot.get(i) == 1) akcja = "L";
            else if(rot.get(i) == 2) akcja = "U";
            else if(rot.get(i) == 3) akcja = "D";
            else if(rot.get(i) == 4) akcja = "F";
            else if(rot.get(i) == 5) akcja = "B";

            obrocWarstwe(akcja, Math.PI/2, true);
            abstractCube.Rotation(akcja);
        }
    }
}
