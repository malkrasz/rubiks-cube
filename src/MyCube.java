import com.sun.j3d.utils.geometry.Box;
import java.util.Vector;
import javax.media.j3d.Appearance;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Małgorzata
 */
public class MyCube extends Box {
    
    public int nr;
    public double katx;
    public double katy;
    public double katz;
    public Vector<Double> katObrotu;
    public Vector<Character> ktoraOs;
    public Vector<Character> obrot;

    MyCube(float size, int number, int flag, Appearance app){
        super(size, size, size, flag, app);
       
        for (int i = 0; i < 6; i++) {
            this.getChild(i).setUserData(number);
        }
        
        nr = number;
        this.katx = 0;
        this.katy = 0;
        this.katz = 0;
        this.katObrotu = new Vector<Double>();
        this.ktoraOs = new Vector<Character>();
        this.obrot = new Vector<Character>();
    }

    public void HowYouAre(){
        System.out.print(nr);
    }
}
