import com.sun.j3d.utils.behaviors.vp.OrbitBehavior;
import com.sun.j3d.utils.pickfast.*;
import javax.media.j3d.*;
import javax.swing.*;
import java.awt.*;
import com.sun.j3d.utils.universe.SimpleUniverse;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.media.j3d.Transform3D;
import javax.vecmath.Vector3f;


public class RubikCube extends MouseAdapter implements KeyListener
{
    private int[][][] kostka = new int[3][3][3];
    public PickCanvas pickCanvas;
    
    public CubeRotation cubeRotation;
    public AbstractCube abstractCube;
    private MouseManual Myszka;
            
    public BranchGroup scena = new BranchGroup();
    public SimpleUniverse simpleU;
    private ArrayList<TransformGroup> kosteczki = new ArrayList<TransformGroup>();
    public Canvas3D canvas3D;

    
    RubikCube() {

        // Tworzenie okienka programu
        JFrame frame = new JFrame("Rubik's Cube");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);

        GraphicsConfiguration config = SimpleUniverse.getPreferredConfiguration();
        canvas3D = new Canvas3D(config);
        canvas3D.setPreferredSize(new Dimension(800,600));

        scena = utworzScene();
        scena.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);

        simpleU = new SimpleUniverse(canvas3D);

        Transform3D przesuniecie_obserwatora = new Transform3D();
        przesuniecie_obserwatora.set(new Vector3f(0.0f,0.0f,3.0f));

        simpleU.getViewingPlatform().getViewPlatformTransform().setTransform(przesuniecie_obserwatora);

        simpleU.addBranchGraph(scena);

        // Obrót kamery za pomocą myszki
        OrbitBehavior orbit = new OrbitBehavior(canvas3D, OrbitBehavior.REVERSE_ROTATE);
        orbit.setSchedulingBounds(new BoundingSphere());
        simpleU.getViewingPlatform().setViewPlatformBehavior(orbit);

        frame.add(canvas3D);
        pickCanvas = new PickCanvas(canvas3D, scena);
        pickCanvas.setTolerance(5.0f);
        pickCanvas.setMode(PickInfo.PICK_GEOMETRY);
        pickCanvas.setFlags(PickInfo.LOCAL_TO_VWORLD | PickInfo.CLOSEST_GEOM_INFO | PickInfo.NODE);

        // Dodawanie funkcji obsługujących klawiaturę oraz myszkę
        Myszka = new MouseManual(this);
        
        canvas3D.addMouseListener(this);
        canvas3D.addMouseMotionListener(this);
        canvas3D.addKeyListener(this);
        
        frame.pack();
        frame.setVisible(true);
    }

    
    BranchGroup utworzScene(){

        // Tworzenie sceny, w której znajduje się kostka Rubika
        BranchGroup wezel_scena = new BranchGroup();
        wezel_scena.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
        wezel_scena.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
        wezel_scena.setCapability(BranchGroup.ALLOW_DETACH);

        new CubeCreator(kosteczki, kostka);
        
        cubeRotation = new CubeRotation(kostka, kosteczki);
        abstractCube = new AbstractCube(kostka);
        
        for (TransformGroup element : kosteczki) {
            wezel_scena.addChild(element);
        }
        
        // Tasowanie kostki
        cubeRotation.Shuffle(abstractCube);  

        return wezel_scena;
    }


    // Obsługa przycisków klawiatury
    public void keyPressed(KeyEvent e) {
        System.out.println("you Clicked me");

        // Tasowanie kostki
        if(e.getKeyChar() == 's') {
            cubeRotation.Shuffle(abstractCube);  
        }
        
        // Obrót prawej warstwy kostki
        if(e.getKeyChar() == 'r') {
            cubeRotation.obrocWarstwe("R", Math.PI/2, true);  
            abstractCube.Rotation("R");
        }

        // Obrót górnej warstwy kostki
        if(e.getKeyChar() == 'u') {
            cubeRotation.obrocWarstwe("U", Math.PI/2, true);  
            abstractCube.Rotation("U");
        }

        // Obrót dolnej warstwy kostki
        if(e.getKeyChar() == 'd') {
            cubeRotation.obrocWarstwe("D", Math.PI/2, true); 
            abstractCube.Rotation("D");
       }

        // Obrót lewej warstwy kostki
        if(e.getKeyChar() == 'l') {
            cubeRotation.obrocWarstwe("L", Math.PI/2, true); 
            abstractCube.Rotation("L");
        }

        // Obrót przedniej warstwy kostki
        if(e.getKeyChar() == 'f') {
            cubeRotation.obrocWarstwe("F", Math.PI/2, true); 
            abstractCube.Rotation("F");
        }

        // Obrót tylnej warstwy kostki
        if(e.getKeyChar() == 'b') {
            cubeRotation.obrocWarstwe("B", Math.PI/2, true); 
            abstractCube.Rotation("B");
        }
        
        /*
        if(e.getKeyChar() == 'p') {
            cubeRotation.previousMove(abstractCube);
        }
        
        if(e.getKeyChar() == 'n') {
            
        }
        */
        
        // Blokowanie obrotu kamery
        if(e.getKeyChar() == 'z'){
            simpleU.getViewingPlatform().setViewPlatformBehavior(null);
        }
        
        // Aktywowanie obrotu kamery
        if(e.getKeyChar() == 'x'){
            OrbitBehavior orbit = new OrbitBehavior(canvas3D, OrbitBehavior.REVERSE_ROTATE);
            orbit.setSchedulingBounds(new BoundingSphere());
            simpleU.getViewingPlatform().setViewPlatformBehavior(orbit);
        }
        
    }


    // Obsługa myszki
   public void mousePressed(MouseEvent e)
   {
        System.out.println(Myszka.pozycjaMyszkiNaKostce(e));
        Myszka.mousePressed(e);
   }

   public void mouseDragged(MouseEvent e){
        Myszka.mouseDragged(e);
   } 
   
   public void mouseReleased(MouseEvent e){
        Myszka.mouseReleased(e);
   }


   // Główna funkcja programu
   public static void main(String args[]){
        new RubikCube();
   }


    @Override
    public void keyTyped(KeyEvent e) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyReleased(KeyEvent e) {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    } 

}